# Nornir Utilities for common use cases

from nornir import InitNornir
import socket
import os
from getpass import getpass


def initialise_nornir(host_file, group_file, num_of_threads=5):
    """
    This fuction will initialise the nornir object bsed on the
    calling arguments. The host & group file names will be
    looked into inventory directory by default.
    Args:
        host_file ([str]): Nornir Host Inventory File name
        group_file ([str]): Nornir Group Inventory File name
        num_of_threads (int, optional): The number of threads to be used during script
                                        execution. Defaults to 5.

    Returns:
        [class]: Nornir object for program execution.
    """
    host_path = f"{os.path.dirname(__file__)}/inventory/{host_file}"
    group_path = f"{os.path.dirname(__file__)}/inventory/{group_file}"
    try:
        os.path.isfile(host_path)
        print("Proccessing Host Inventory file")
    except IOError as err:
        print("I/O error({0}): {1}".format(err.errno, err.strerror))
    try:
        os.path.isfile(group_path)
        print("Proccessing Group Inventory file\n")
    except IOError as err:
        print("I/O error({0}): {1}".format(err.errno, err.strerror))

    return InitNornir(
        runner={
            "plugin": "threaded",
            "options": {
                "num_workers": num_of_threads,
            },
        },
        inventory={
            "plugin": "SimpleInventory",
            "options": {
                "host_file": host_path,
                "group_file": group_path,
            },
        },
    )


def nornir_set_creds(norn, username=None, password=None):
    """
    Handler for handling inventory credentials i.e. username
    & password. With this function we don't need to store
    this credentials in clear text in the inventory.
    Args:
        norn ([obj]): Nornir object with config initliased
        username ([str], optional): [Device Username]. Defaults to None.
        password ([try], optional): [Device Password]. Defaults to None.
    """
    if not username:
        username = input("Please Enter Device Admin Access username: ")
    if not password:
        password = getpass("Please Enter Device Admin Access Password: ")
    for host_obj in norn.inventory.hosts.values():
        host_obj.username = username
        host_obj.password = password
    print(
        f"Sucessfully Assigned Device Credentails to {len(norn.inventory.hosts)} Devices\n"
    )


def ssh_host_check(host):
    """
    This Fuction checks the SSH Port 22 availablity on the
    host. If device accepts SSH port, it returns True else
    returns False.
    Args:
        host ([str]): [IP Address of the Device]
        port ([int]): tcp/port numbder. Default to 22.
    """
    # Initialse Socket Object for IPv4 Address Family
    port = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Set timeout if SSH Port is not open.
    port.settimeout(2)
    try:
        # Open SSH Conection to the device
        port.connect((host, 22))
        return True
    except:
        return False
