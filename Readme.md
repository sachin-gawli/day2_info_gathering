This Script will help to gather information from network device
& store this information for each host.

The information file for each host will be saved under "BACKUP"
folder under current working directory. Each file will be
timestamped for easy compare.

This script uses python nornir framework & netmiko module to
gather information from network devices listed under inventory.
The various show commands are decoulped from main script so
that we can add any additiona commands in future. The show
commands are stored under "info_cmds.yml" file.

The commands which will be executed on the devices depends on
"device_type" key value of each host from inventory. Based on
this key value, the show commands are catogoried into three
types under "info_cmds.yml" file as below:

- switch
- router
- nxos

The nornir will by default look into "inventory" folder while
initialising nornir. The host & group inventory files can be
passed by calling fuction which allows different or multiple
inventory files to be used during each execution based project.

As a best practice, the device username/password are not stored
as clear text under inventory file, instead this information
will be provided during run time of the script.

The sample files are stored under "BACKUP" folder for ref.

Tested with python Version 3.8+

Required Packages using python venv:

- pip3 install nornir
- pip3 install nornir_netmiko

Happy Coding !!!!
