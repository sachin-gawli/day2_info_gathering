import nornir_utilities
from nornir.core.filter import F
from nornir_utils.plugins.functions import print_result
from nornir_netmiko import netmiko_send_command

import os
from datetime import datetime
import yaml

current_dir = os.path.dirname(__file__)
backup_folder = os.path.dirname(__file__) + "/BACKUP"
cmd_timestamp = datetime.now().strftime("%d-%m-%Y : %H:%M:%S")
file_timestamp = datetime.now().strftime("%d_%m_%Y-%H_%M")

info_template = """

############################################################
{:30} : {:^30}
############################################################

{}
"""


def create_backup_folder():
    """
    This Function creates "BACKUP" folder under current working
    directory if it does not exists already
    """
    if not os.path.exists(backup_folder):
        print("Creating Backup folder in the current working directory\n")
        os.makedirs(backup_folder)
    else:
        print("Backup folder already exists\n")


def save_info_file(host, content):
    info_file_name = f"{host}_{file_timestamp}_info.txt"
    with open(os.path.join(backup_folder, info_file_name), "a") as file:
        file.write(content)


def get_router_info(task):
    for cmd in info_cmds["router"]:
        output = task.run(task=netmiko_send_command, command_string=cmd)
        content = info_template.format(cmd, cmd_timestamp, output.result)
        save_info_file(task.host, content)


def get_sw_info(task):
    for cmd in info_cmds["switch"]:
        output = task.run(task=netmiko_send_command, command_string=cmd)
        content = info_template.format(cmd, cmd_timestamp, output.result)
        save_info_file(task.host, content)


def get_nxos_info(task):
    for cmd in info_cmds["nxos"]:
        output = task.run(task=netmiko_send_command, command_string=cmd)
        content = info_template.format(cmd, cmd_timestamp, output.result)
        save_info_file(task.host, content)


def get_info(task):
    # Check if SSH Port is open on the target host
    ssh_status = nornir_utilities.ssh_host_check(task.host.hostname)
    if ssh_status:
        if task.host["device_type"].lower() == "router":
            print(f"Collecting Information for {task.host} Device\n")
            task.run(task=get_router_info)
            print(f"Saving Information for {task.host} Device\n")
        elif task.host["device_type"].lower() == "switch":
            print(f"Collecting Information for {task.host} Device\n")
            task.run(task=get_sw_info)
            print(f"Saving Information for {task.host} Device\n")
        elif task.host["device_type"].lower() == "nxos":
            print(f"Collecting Information for {task.host} Device\n")
            task.run(task=get_nxos_info)
            print(f"Saving Information for {task.host} Device\n")
    else:
        print(f"{task.host} is not Reachable")


if __name__ == "__main__":
    # Open yml file containing info commands from current working directory.
    with open(os.path.join(current_dir, "info_cmds.yml"), "r") as file:
        print("Loading Info Commands from File\n")
        info_cmds = yaml.safe_load(file)
    # Create Backup Folder under Current Working Directory for saving info
    # files per host under inventory.
    create_backup_folder()

    # Initialise nornir object.
    nr = nornir_utilities.initialise_nornir("hosts.yml", "groups.yml")

    # Filter Target hosts from Inventory for info gathering
    target_hosts = nr.filter(F(tags__contains="all"))

    # Set Device Credentials for login
    nornir_utilities.nornir_set_creds(target_hosts)

    # Run info task against filtered target hosts.
    result = target_hosts.run(task=get_info)
    # print_result(result)

    # Print host details if task has failed for future troubleshooting
    if result.failed:
        for host in result.failed_hosts:
            print(f"The Task has failed on {host} Device")
